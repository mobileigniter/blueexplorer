# Bluetooth Explorer #
This project is a wrapper for a couple of Bluetooth plugins  (see https://github.com/randdusing/BluetoothLE and https://github.com/tanelih/phonegap-bluetooth-plugin) to show how Bluetooth could be incorporated into a phone app.

Bluetooth Classic only works on Android OS. Apple iOS is not supported because they require MFi approval for Bluetooth Classic.

Bluetooth Low Energy works with Android OS > 4.2 or iOS >= 7.

Bluetooth Classic is better for projects with high data throughput (can be up to 10 times faster at streaming data than Bluetooth Low Energy). Bluetooth Low Energy is better for devices with strict power usage requirements or situations (such as beacons) where passive data advertisement is preferable to two-way communication.

Special build instructions for iOS: after doing "ionic state reset", you might need to do "cordova plugin remove cordova-plugin-bluetoothle" followed by "cordova plugin add cordova-plugin-bluetoothle" to get bluetooth to actually work.

# Mobile Igniter Ionic Side-Menu Seed Project #

* If you're just getting started, begin with the ionic's [getting started]() if you haven't done that yet:

* Standard [info for blank ionic project](https://github.com/driftyco/ionic-starter-sidemenu/blob/master/README.md)

##About this project:##

This is a seed project based on the ionic side-menu project, intended to lay out a basic framework for building a new application. If you want to allow pulling from this repo (accept project-wide changes/build tweaks, etc), start your new project by forking from this repo and [you can pull from it later](http://stackoverflow.com/questions/4169832/update-my-github-repo-which-is-forked-out-from-another-project). The best way to add gulp tasks without potentially creating merge conflicts with this parent repo is to just add new files under gulp-tasks, because the gulpfile is configured to pull in all files in the dir.

Otherwise you can just clone this repo and use it as is.

###Code Structure/Organization: ###

* Follow [best practices](https://github.com/toddmotto/angularjs-styleguide) with your angular code
* Gulp Watch|dev environment build process grabs \*.js in www/js/\*\*/\*, www/views/\*\*/\*, and concats everything into www/js/_compiled (don't edit files there...)
* Android|iOS build process grabs \*.html in www/views/\*\*/\*, www/js/directives/\*\*/\*.html and www/templates/\*\*/\*.html (for directives, modals, and such)
* Organize controllers, views, and stateconfig into directories in views (see example for 'Home'). It's alright to have multiple views/controllers/view states in one view subdir, just make sure that's all contained in the views/dir
* Make your directives self-contained in www/js/directives (js, html, and eventually css--but not yet)
* To make a new angular module, make sure to declare the module in app.js, and only refer to it with getters (`angular.module('myModule')`) in your module's .js

###Getting Started###

Make sure to run `npm install` && `bower install` then just `ionic serve` from your directory and you're good to go.

Update app_id, dev_app_id and name in ionic.project. The build process will grab them for versioning production and dev versions of the app.

###Ionic IO Setup###

Ionic IO Push and Ionic IO Deploy libraries are already in place, but you'll need to do the other config steps. You can find info about them by reading the guides here ([push](http://docs.ionic.io/docs/push-from-scratch), [deploy](http://docs.ionic.io/docs/deploy-quick-start)).

Notably, you'll need to run `ionic io init` (make sure you changed app_id first), and then do the appropriate Android or iOS setup.

###Build Steps###

* If you want to build for device, make sure you have the appropriate dev tools (Android/iOS SDK) configured--refer to above Ionic readme for setting up your dev station *
* If you're adding a cordova plugin, delete the entire target env build (build/dev|qa|prod) entirely and run build command

* To build iOS or Android: `cordova build|run android|ios [--device] [--prod (dev by default)] [-i=version_number (-v and --version are taken)]`
(defaults to build --dev -i=alpha)
** --prod build will uglify all js, and use app_id instead of dev_app_id
** -i is for internal purposes only
