(function () {
  'use strict';
  var connectConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('app.classicConnect', {
        url: "/classicconnect/:deviceAddress",
        views: {
          main: {
            templateUrl: "views/Connect/classicConnect.html",
            controller: 'classicConnectController'
          }
        }
      });
  }],
    connectCtrl = ['$ionicLoading', '$timeout', 'bluething', '$scope', '$stateParams', '$ionicPopup', '$q', function ($ionicLoading, $timeout, bluething, $scope, $stateParams, $ionicPopup, $q) {
      var loadingMessage = function (message, button) {
        var loadScreen = {template: "<ion-spinner></ion-spinner><div>" + message + "</div>", delay: 100};
        if (button) {
          loadScreen.template += '<button class="button button-block icon-left ion-close-circled assertive" ng-click="disconnect()"><b>Cancel</b></button>';
          loadScreen.scope = $scope;
        }
        return loadScreen;
      };

      $scope.write = function () {
        window.StatusBar.hide();
        $ionicLoading.show(loadingMessage("Sending Message..."));
        bluething.classicWrite($scope.vm.newMessage).then(function (success) {
          console.log("classic write success " + JSON.stringify(success));
          $ionicLoading.hide();
        }, function (err) {
          console.log("classic write fail " + JSON.stringify(err));
          $ionicLoading.hide();
        });
        $scope.outMessage = $scope.outMessage.concat($scope.vm.newMessage);
        $ionicLoading.hide();
      };

      $scope.disconnect = function (error) {
        window.StatusBar.hide();
        if (error) {
          $ionicPopup.alert({title: 'Connection error', template: JSON.stringify(error)});
        } else {
          $ionicLoading.show(loadingMessage("Disconnecting..."));
        }
        bluething.disconnect();
        $scope.connected = false;
        $timeout(function () {
          $ionicLoading.hide();
        }, 300);
      };

      $scope.vm = {};
      $scope.connect = function () {
        var promise = $q.defer();

        $ionicLoading.show(loadingMessage("Connecting...", true));
        $scope.inMessage = "";
        $scope.outMessage = "";
        $scope.vm.newMessage = "";
        bluething.classicConfigure($stateParams.deviceAddress).then(function (uuids) {
          var uuidChoice = [];

          console.log("UUIDs " + JSON.stringify(uuids));
          if (uuids.length > 1) {
            uuids.forEach(function (id) {
              uuidChoice.push({text: id, onTap: function () {
                return id;
              }});
            });
            $ionicLoading.hide();
            $ionicPopup.show({
              title: 'Choose a UUID',
              cssClass: "popup-vertical-buttons",
              buttons: uuidChoice
            }).then(function (res) {
              $ionicLoading.show(loadingMessage("Connecting..."));
              promise.resolve(res);
            });
          } else if (uuids.length === 1) {
            promise.resolve(uuids[0]);
          } else {
            promise.resolve('00001101-0000-1000-8000-00805f9b34fb');
          }
        }, promise.reject);

        promise.promise.then(function (uuid) {
          bluething.classicConnect($stateParams.deviceAddress, uuid, function (message) {
            console.log("the device said: " + JSON.stringify(message));
            $scope.$apply(function () {
              $scope.inMessage = $scope.inMessage.concat(message);
            });
          }).then(function () {
            console.log("connected");
            $ionicLoading.hide();
          }, $scope.disconnect);
        }, $scope.disconnect);
        $scope.connected = true;
      };

      bluething.stopScans();
      $scope.connect();
    }];
  angular.module('Home')
    .config(connectConfig)
    .controller('classicConnectController', connectCtrl);
}());
