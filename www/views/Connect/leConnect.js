(function () {
  'use strict';
  var connectConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('app.leConnect', {
        url: "/leconnect/:deviceAddress",
        views: {
          main: {
            templateUrl: "views/Connect/leConnect.html",
            controller: 'leConnectController'
          }
        }
      });
  }],
    connectCtrl = ['$ionicPopup', '$ionicLoading', '$timeout', 'bluething', '$scope', '$stateParams', '$ionicScrollDelegate', function ($ionicPopup, $ionicLoading, $timeout, bluething, $scope, $stateParams, $ionicScrollDelegate) {
      var loadingMessage = function (message, button) {
        var loadScreen = {template: "<ion-spinner></ion-spinner><div>" + message + "</div>", delay: 100};
        if (button) {
          loadScreen.template += '<button class="button button-block icon-left ion-close-circled assertive" ng-click="disconnect()"><b>Cancel</b></button>';
          loadScreen.scope = $scope;
        }
        return loadScreen;
      }, myHide = function () {
        $ionicLoading.hide();
        $timeout(function () {
          $ionicLoading.hide();//Ionic can be annoying--sometimes, loading isn't closed on the first attempt.
        }, 150);
      };

      $scope.write = function () {
        window.StatusBar.hide();
        $ionicLoading.show(loadingMessage("Sending Message..."));
        bluething.leWrite($scope.vm.newMessage).then(function (success) {
          console.log("LE write success " + JSON.stringify(success));
          myHide();
        }, function (err) {
          $ionicPopup.alert({title: 'Err', template: JSON.stringify(err)});
          myHide();
        });
        $scope.outMessage = $scope.outMessage.concat($scope.vm.newMessage);
        myHide();
      };

      $scope.disconnect = function (error) {
        window.StatusBar.hide();
        if (error) {
          $ionicPopup.alert({title: 'Connection error', template: JSON.stringify(error)});
        } else {
          $ionicLoading.show(loadingMessage("Disconnecting..."));
        }
        bluething.disconnect();
        $scope.connected = false;
        $scope.configurationState = -1;
        $timeout(function () {
          $ionicLoading.hide();
        }, 300);
      };

      $scope.chooseWrite = function (uuid) {
        $scope.configurationState = 3;
        $ionicLoading.show(loadingMessage("Configuring..."));
        $ionicScrollDelegate.scrollTop();
        console.log("UUIDs: " + JSON.stringify($scope.readUuid) + " " + JSON.stringify(uuid));
        bluething.leConfigure(uuid, $scope.readUuid, function (message) {
          console.log("the device said: " + JSON.stringify(message));
          $scope.$apply(function () {
            $scope.inMessage = $scope.inMessage.concat(message);
          });
        }).then(function () {
          myHide();
        }, $scope.disconnect);
      };

      $scope.chooseRead = function (uuid) {
        $scope.readUuid = uuid;
        $scope.configurationState = 2;
        $ionicPopup.alert({title: 'Configuration', template: 'Choose a characteristic to write.'});
      };

      $scope.next = function () {
        $scope.configurationState = 1;
        $ionicPopup.alert({title: 'Configuration', template: 'Choose a characteristic from which to read.'});
      };

      $scope.readOnce = function (uuid) {
        if (uuid.descriptorUuid) {
          $ionicLoading.show(loadingMessage("Reading descriptor..."));
        } else {
          $ionicLoading.show(loadingMessage("Reading characteristic..."));
        }
        bluething.leReadOnce(uuid).then(function (value) {
          myHide();
          $ionicPopup.alert({title: value});
        }, function (err) {
          myHide();
          $ionicPopup.alert({title: 'Err', template: JSON.stringify(err)});
        });
      };

      $scope.vm = {};
      $scope.connect = function () {
        $ionicLoading.show(loadingMessage("Connecting...", true));
        $scope.inMessage = "";
        $scope.outMessage = "";
        $scope.vm.newMessage = "";
        $scope.configurationState = 0;
        bluething.leConnect($stateParams.deviceAddress).then(function (services) {
          console.log("connected " + JSON.stringify(services));
          $scope.services = services;
          myHide();
          $ionicPopup.alert({title: 'Configuration', template: "Click to do one-time reads. Hit 'Next' to subscribe to read characteristics."});
        }, $scope.disconnect);
        $scope.connected = true;
      };

      bluething.stopScans();
      $scope.connect();
    }];
  angular.module('Home')
    .config(connectConfig)
    .controller('leConnectController', connectCtrl);
}());
