(function () {
  'use strict';
  var scanConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('app.leScan', {
        url: "/scan",
        views: {
          main: {
            templateUrl: "views/Scan/leScan.html",
            controller: 'leScanController'
          }
        }
      });
  }],
    scanCtrl = ['$ionicLoading', '$timeout', 'bluething', '$scope', '$localStorage', function ($ionicLoading, $timeout, bluething, $scope, $localStorage) {
      var loadingMessage = function (message) {
        return {template: "<ion-spinner></ion-spinner><div>" + message + "</div>"};
      };

      $scope.erase = function () {
        $scope.deviceList = {newer: [], older: []};
        $localStorage.leDeviceList = {newer: [], older: []};
      };

      $scope.info = function (device) {
        bluething.leAdInfo(device);
      };

      $scope.toggleDevice = function (device, newer) {
        if (newer) {
          $scope.deviceList.newer[device].toggled = !$scope.deviceList.newer[device].toggled;
        } else {
          $scope.deviceList.older[device].toggled = !$scope.deviceList.older[device].toggled;
        }
      };

      $scope.stop = function () {
        $ionicLoading.show(loadingMessage("Stopping..."));
        bluething.stopScans();
        $scope.scanning = false;
        $timeout(function () {
          $ionicLoading.hide();
        }, 300);
      };

      $scope.start = function () {
        var i;
        if (!$localStorage.leDeviceList) {
          $scope.deviceList = {newer: [], older: []};
        } else {
          $scope.deviceList = {newer: []};
          $scope.deviceList.older = $localStorage.leDeviceList.newer.concat($localStorage.leDeviceList.older);
        }
        $scope.scanning = true;

        bluething.leScan(function (device) {
          var found = -1,
            makeOldNew = function () {
              device.toggled = $scope.deviceList.older[found].toggled;
              $scope.deviceList.older.splice(found, 1);
              $scope.deviceList.newer.push(device);
              $localStorage.leDeviceList = $scope.deviceList;
            },
            updateNew = function () {
              if (found > -1) {
                device.toggled = $scope.deviceList.newer[found].toggled;
                $scope.deviceList.newer[found] = device;
              } else {
                $scope.deviceList.newer.push(device);
              }
              $localStorage.leDeviceList = $scope.deviceList;
            };
          for (i = 0; i < $scope.deviceList.older.length; i += 1) {
            if ($scope.deviceList.older[i].address === device.address) {
              found = i;
              $scope.$apply(makeOldNew);
              return;
            }
          }
          for (i = 0; i < $scope.deviceList.newer.length; i += 1) {
            if ($scope.deviceList.newer[i].address === device.address) {
              found = i;
              break;
            }
          }
          $scope.$apply(updateNew);
        }, function () {
          $scope.scanning = false;
        });
      };

      window.StatusBar.hide();
      bluething.disconnect();
      $scope.start();
    }];
  angular.module('Home')
    .config(scanConfig)
    .controller('leScanController', scanCtrl);
}());
