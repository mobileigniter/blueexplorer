(function () {
  'use strict';
  var scanConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('app.classicDiscover', {
        url: "/discover",
        views: {
          main: {
            templateUrl: "views/Scan/classicScan.html",
            controller: 'classicScanController'
          }
        }
      });
  }],
    scanCtrl = ['$ionicLoading', '$timeout', 'bluething', '$scope', '$localStorage', function ($ionicLoading, $timeout, bluething, $scope, $localStorage) {
      var loadingMessage = function (message) {
        return {template: "<ion-spinner></ion-spinner><div>" + message + "</div>"};
      };

      $scope.erase = function () {
        $scope.deviceList = {newer: [], older: []};
        $localStorage.classicDeviceList = {newer: [], older: []};
      };

      $scope.stop = function () {
        $ionicLoading.show(loadingMessage("Stopping..."));
        bluething.stopScans();
        $scope.scanning = false;
        $timeout(function () {
          $ionicLoading.hide();
        }, 300);
      };

      $scope.start = function () {
        var i;
        if (!$localStorage.classicDeviceList) {
          $scope.deviceList = {newer: [], older: []};
        } else {
          $scope.deviceList = {newer: []};
          $scope.deviceList.older = $localStorage.classicDeviceList.newer.concat($localStorage.classicDeviceList.older);
        }
        $scope.scanning = true;

        bluething.classicDiscover(function (device) {
          var found = -1,
            makeOldNew = function () {
              $scope.deviceList.older.splice(found, 1);
              $scope.deviceList.newer.push(device);
              $localStorage.classicDeviceList = $scope.deviceList;
            },
            updateNew = function () {
              if (found > -1) {
                $scope.deviceList.newer[found] = device;
              } else {
                $scope.deviceList.newer.push(device);
              }
              $localStorage.classicDeviceList = $scope.deviceList;
            };
          for (i = 0; i < $scope.deviceList.older.length; i += 1) {
            if ($scope.deviceList.older[i].address === device.address) {
              found = i;
              $scope.$apply(makeOldNew);
              return;
            }
          }
          for (i = 0; i < $scope.deviceList.newer.length; i += 1) {
            if ($scope.deviceList.newer[i].address === device.address) {
              found = i;
              break;
            }
          }
          $scope.$apply(updateNew);
        }, function () {
          //$ionicPopup.alert({title: 'Done', template: "Bluetooth inquiry complete."});
          $scope.$apply(function () {
            $scope.scanning = false;
          });
        });
      };

      window.StatusBar.hide();
      bluething.disconnect();
      $scope.start();
    }];
  angular.module('Home')
    .config(scanConfig)
    .controller('classicScanController', scanCtrl);
}());