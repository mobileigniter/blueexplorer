(function () {
  'use strict';
  var homeConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('app.home', {
        url: "/home",
        views: {
          main: {
            templateUrl: "views/Home/Home.html",
            controller: 'HomeController as vm'
          }
        }
      });
  }],
    homeCtrl = ['$ionicLoading', '$q', 'bluething', '$ionicPopup', function ($ionicLoading, $q, bluething, $ionicPopup) {
      var vm = this,
        initLoop,
        loadingMessage = function (message) {
          return {template: "<ion-spinner></ion-spinner><div>" + message + "</div>", delay: 100};
        };

      $ionicLoading.show(loadingMessage("Initializing Bluetooth"));
      vm.init = function () {
        var leWait = $q.defer(),
          classicWait = $q.defer(),
          timerWait = $q.defer();

        $ionicLoading.show(loadingMessage("Initializing Bluetooth"));
        vm.initFailed = false;
        vm.classicReady = false;
        vm.leReady = false;

        if (window.cordova && window.cordova.plugins && window.cordova.plugins.diagnostic) {
          window.cordova.plugins.diagnostic.isLocationAuthorized(function (enabled) {
            console.log("enabled? " + JSON.stringify(enabled));
            if (!enabled) {
              //This diagnostic plugin is a little weird--it has a tendency to say that permissions are denied
              //even when they aren't. Don't bother putting in the $ionicPopups--they end up being super annoying.
              //After calling requestLocationAuthorization, Android permission request modal will only show up if permission
              //is actually not been granted.
              // $ionicPopup.alert({'title': 'This app needs to request location permissions.',
              //   'template': 'Bluetooth signal strength can be used to determine your location relative to other, nearby devices.'});
              window.cordova.plugins.diagnostic.requestLocationAuthorization(function (status) {
                console.log("location status " + JSON.stringify(status));
                // if (status !== window.cordova.plugins.diagnostic.runtimePermissionStatus.GRANTED) {
                //   $ionicPopup.alert({'title': 'Bluetooth will not work', 'template': 'location permission = ' + JSON.stringify(status)});
                // }
              }, function (err) {
                $ionicPopup.alert({'title': 'Unexpected error: Bluetooth may not work', 'template': JSON.stringify(err)});
              });
            } else {
              console.log("location permissions enabled");
            }
          }, function (err) {
            $ionicPopup.alert({'title': 'Unexpected error: Bluetooth may not work', 'template': JSON.stringify(err)});
          });
        } else {
          console.warn("Could not determine permissions status.");
        }

        bluething.leInit().then(function () {
          console.log("LE ready");
          vm.leReady = true;
          leWait.resolve();
        }, function (err) {
          console.log("LE init error " + JSON.stringify(err));
          leWait.resolve();
        });

        bluething.classicInit().then(function () {
          console.log("Classic ready");
          vm.classicReady = true;
          classicWait.resolve();
        }, function (err) {
          console.log("Classic init error " + JSON.stringify(err));
          classicWait.resolve();
        });

        setTimeout(function () {
          timerWait.resolve();
        }, 500);

        $q.all([leWait.promise, classicWait.promise, timerWait.promise]).then(function () {
          $ionicLoading.hide();
          vm.initFailed = !vm.leReady && !vm.classicReady;
        });
      };

      initLoop = setInterval(function () {
        //ionic.Platform.ready can't be used more than once on an iPad and it's already used in app.js
        if (ionic.Platform.isReady) {
          clearInterval(initLoop);
          bluething.stopScans();
          bluething.disconnect();
          vm.init();
        } else {
          console.log("not ready yet");
        }
      }, 500);
    }];
  angular.module('Home')
    .config(homeConfig)
    .controller('HomeController', homeCtrl);
}());
