(function () {
  'use strict';
  var appRun = ['$ionicPlatform', '$rootScope', '$ionicLoading', '$http', '$timeout', '$ionicPopup', '$ionicAnalytics',
      function ($ionicPlatform, $rootScope, $ionicLoading, $http, $timeout, $ionicPopup, $ionicAnalytics) {
        var stateTimeout = {};
        $ionicPlatform.ready(function () {
          $ionicAnalytics.register();

          ionic.Platform.isFullScreen = true; /// fix keyboard covering inputs
          if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            window.StatusBar.styleDefault();
          }
          if (ionic.Platform.isWebView()) {
            navigator.splashscreen.hide();
            /// $ionicDeploy only works within an app

            // NOT USING
//            $ionicDeploy.check().then(function (hasUpdate) {
//              console.log('Ionic Deploy: Update available: ' + hasUpdate);
//              // $scope.hasUpdate = hasUpdate;
//              // $ionicDeploy.update().then(function(res) {
//              //   console.log('Ionic Deploy: Update Success! ', res);
//              // }, function(err) {
//              //   console.log('Ionic Deploy: Update error! ', err);
//              // }, function(prog) {
//              //   console.log('Ionic Deploy: Progress... ', prog);
//              // });
//            }, function (err) {
//              console.error('Ionic Deploy: Unable to check for updates', err);
//            });
          }
        });
        /*jslint unparam: true*/
        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
          $ionicLoading.hide();
          // if (error && error.status === 401) { /// invalid token
          //   $ionicPopup.alert({'title': 'Invalid Access Token', 'template': 'Your token has expired or is otherwise invalid. Please log in again.'});
          //   // $rootScope.signOut(toState.name);
          //   $rootScope.signOut();
          // } else {
          $ionicPopup.alert({'title': 'Unexpected Error', 'template': 'Failed to connect to cloud. Please try again.'});
          // }
          $rootScope.cancelTimeout();
        });
        /*jslint unparam:false */
        $rootScope.$on('$stateChangeStart', function () {
          stateTimeout = $timeout(function () { /// if transition takes longer than 30 seconds, timeout
            $ionicLoading.hide();
            // $ionicPopup.alert({'title': 'Timed Out', 'template': 'Communication with the server timed out. Please check your connection and try again.'});
            angular.forEach($http.pendingRequests, function (req) {
              if (req.abort) {
                req.abort.resolve();
              }
            });
          }, 30000);
        });
        $rootScope.$on('$stateChangeSuccess', function () {
          $rootScope.cancelTimeout();
        });
        $rootScope.cancelTimeout = function () {
          $timeout.cancel(stateTimeout);
        };
        $rootScope.closeLoading = function () {
          $ionicLoading.hide();
          angular.forEach($http.pendingRequests, function (req) {
            if (req.abort) {
              req.abort.resolve();
            }
          });
        };
      }],
    appConfig = ['$stateProvider', '$urlRouterProvider', '$httpProvider', '$ionicLoadingConfig',
      function ($stateProvider, $urlRouterProvider, $httpProvider, $ionicLoadingConfig) {
        $stateProvider
          .state('app', {
            url: "/app",
            abstract: true,
            templateUrl: "templates/menu.html",
            controller: "appController as appVM"
          });
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/home');
        //// add an http aborter
        $httpProvider.interceptors.push(['$q', function ($q) {
          return {
            request: function (config) {
              var abort = $q.defer();
              config.abort = abort;
              config.timeout = abort.promise;
              return config;
            }
          };
        }]);
        /// customize ionicLoading, add cancel button
        $ionicLoadingConfig.template = '<ion-spinner></ion-spinner><div>Loading...</div><button class="button button-clear icon ion-close-circled" ng-click="closeLoading()"></button>';
      }],
    appCtrl = [function () {
      angular.noop();
    }];
  angular.module('Home', []);
  angular.module('mi.app', ['ionic', 'ionic.service.core', 'ionic.service.analytics', 'ngCordova', 'ngStorage', 'Home'])
    .config(appConfig)
    .controller('appController', appCtrl)
    .run(appRun);
}());
