(function () {
  'use strict';
  var bluething = ['$ionicPopup', '$q', function ($ionicPopup, $q) {
    var bluer = {},
      iosFailed = false,
      leAvailable = false,
      classicAvailable = false,
      leParams = {},
      hexDisplay = function (x, reverse) {
        var i, r = [];
        if (reverse) {
          for (i = x.length - 1; i > -1; i -= 1) {
            r.push(("00" + x.charCodeAt(i).toString(16)).slice(-2));
          }
        } else {
          for (i = 0; i < x.length; i += 1) {
            r.push(("00" + x.charCodeAt(i).toString(16)).slice(-2));
          }
        }
        return r;
      },
      nonzeroTest = function (x) {
        var i;
        for (i = 0; i < x.length; i += 1) {
          if (x.charCodeAt(i) !== 0) {
            return true;
          }
        }
        return false;
      },
      manufacturerData = function (nibbleRemainder) {
        var parsedNibble = "manufacturer data ",
          nibbleType = hexDisplay(nibbleRemainder.slice(0, 2), true).join("");
        if (nibbleType === "004c") {
          parsedNibble += "(Apple, Inc.): ";
        } else {
          //See https://www.bluetooth.org/en-us/specification/assigned-numbers/company-identifiers
          //for more company ID's that I did not bother to include.
          parsedNibble += "(Company ID = " + nibbleType + "): ";
        }
        parsedNibble += hexDisplay(nibbleRemainder.slice(2)).join(",");
        return parsedNibble;
      },
      parseAd = function (adInfo) {
        var i = 0,
          parsed = [],
          parsedNibble,
          nibbleLength,
          nibbleType,
          nibbleRemainder;
        if (ionic.Platform.isIOS()) {
          //iOS hides most of the advertisement data from any apps.
          nibbleLength = adInfo.length;
          if (nibbleLength === 1) {
            parsedNibble = "One byte, ";
          } else {
            parsedNibble = nibbleLength + " bytes, ";
          }
          parsedNibble += manufacturerData(adInfo);
          parsed.push(parsedNibble);
        } else {
          while (i < adInfo.length) {
            nibbleLength = adInfo.charCodeAt(i);
            if (nibbleLength < 2) {
              nibbleRemainder = adInfo.slice(i);
              if (nonzeroTest(nibbleRemainder)) {
                parsed.push("Error: " + (adInfo.length - i) + " bytes unparsed. " + hexDisplay(nibbleRemainder).join(","));
              }
              break;
            }
            if (nibbleLength === 2) {
              parsedNibble = "One byte, ";
            } else {
              parsedNibble = (nibbleLength - 1) + " bytes, ";
            }

            nibbleType = adInfo.charCodeAt(i + 1);
            nibbleRemainder = adInfo.slice(i + 2, i + 1 + nibbleLength);
            if (nibbleType === 1) {
              parsedNibble += "flags: " + hexDisplay(nibbleRemainder).join("");
            } else if (nibbleType < 8) {
              parsedNibble += "service UUID: " + hexDisplay(nibbleRemainder, true).join("");
            } else if (nibbleType < 10) {
              parsedNibble += "local name: " + nibbleRemainder;
            } else if (nibbleType === 255) {
              parsedNibble += manufacturerData(nibbleRemainder);
            } else {
              //See https://www.bluetooth.org/en-us/specification/assigned-numbers/generic-access-profile
              //for more advertising types that I did not bother to include.
              parsedNibble += "data type (" + nibbleType.toString(16) + "): " + hexDisplay(nibbleRemainder).join(",");
            }
            parsed.push(parsedNibble);
            i += 1 + nibbleLength;
          }
        }
        return parsed.join('<br>');
      };

    bluer.leWrite = function (message) {
      var promise = $q.defer(),
        fragment = message.length - 20;

      //Message needs to be fragmented into 20 byte chunks according to Bluetooth Low Energy protocol.
      if (fragment > 0) {
        bluer.leWrite(message.slice(0, fragment)).then(function () {
          leParams.value = window.btoa(message.slice(fragment));
          window.bluetoothle.write(promise.resolve, promise.reject, leParams);
        }, promise.reject);
      } else {
        leParams.value = window.btoa(message);
        window.bluetoothle.write(promise.resolve, promise.reject, leParams);
      }
      return promise.promise;
    };

    bluer.leReadOnce = function (uuids) {
      var promise = $q.defer(),
        didRead = false;
      uuids.address = leParams.address;
      window.bluetoothle.read(function (success) {
        didRead = true;
        promise.resolve(window.atob(success.value));
      }, promise.reject, uuids);
      setTimeout(function () {
        if (!didRead) {
          promise.reject("Err: failed to read. 5 second timeout.");
        }
      }, 5000);
      return promise.promise;
    };

    bluer.classicWrite = function (message) {
      var promise = $q.defer();
      window.bluetooth.write(promise.resolve, promise.reject, message, "UTF-8");

      return promise.promise;
    };

    bluer.leConfigure = function (writeConfig, readConfig, readCallback) {
      var promise = $q.defer();

      writeConfig.address = leParams.address;
      leParams = writeConfig;
      leParams.type = 'withResponse';
      readConfig.address = leParams.address;
      readConfig.isNotification = true;
      window.bluetoothle.subscribe(function (data) {
        if (data) {
          if (data.status === 'subscribedResult') {
            readCallback(window.atob(data.value));
          } else if (data.status === 'subscribed') {
            promise.resolve();
          }
        }
      }, promise.reject, readConfig);

      return promise.promise;
    };

    bluer.leConnect = function (address) {
      var promise = $q.defer();
      leParams = {};
      leParams.address = address;

      window.bluetoothle.connect(function (status) {
        if (status.status === "connected") {
          window.bluetoothle.discover(function (discovered) {
            promise.resolve(discovered.services);
          }, promise.reject, leParams);
        }
      }, function (error) {
        if (error.message === "Device previously connected, reconnect or close for new device" ||
            error.message === "Device previously connected, reconnect or close for new connection") {
          console.log("attempting reconnect...");
          window.bluetoothle.reconnect(function (status) {
            if (status.status === "connected") {
              window.bluetoothle.discover(function (discovered) {
                promise.resolve(discovered.services);
              }, promise.reject, leParams);
            }
          }, promise.reject, leParams);
        } else {
          promise.reject(error);
        }
      }, leParams);

      return promise.promise;
    };

    bluer.classicConnect = function (address, uuid, readCallback) {
      var promise = $q.defer();

      window.bluetooth.connect(function () {
        window.bluetooth.startConnectionManager(readCallback, readCallback);
        promise.resolve();
      }, promise.reject, {address: address, uuid: uuid});

      return promise.promise;
    };

    bluer.classicConfigure = function (address) {
      var promise = $q.defer();

      window.bluetooth.getUuids(function (uuid) {
        promise.resolve(uuid.uuids);
      }, promise.reject, address);

      return promise.promise;
    };

    bluer.disconnect = function () {
      if (leAvailable) {
        window.bluetoothle.disconnect(function () {
          window.bluetoothle.close(function () {
            console.log("closed LE connection");
          }, function () {
            // empty block
            return;
          }, {address: leParams.address});
        }, function () {
          window.bluetoothle.close(function () {
            console.log("closed LE connection");
          }, function () {
            // empty block
            return;
          }, {address: leParams.address});
        }, {address: leParams.address});
      }
      if (classicAvailable) {
        window.bluetooth.disconnect(function () {
          console.log("closed classic connection");
        });
      }
    };

    bluer.leScan = function (deviceCallback, finalCallback) {
      window.bluetoothle.startScan(function (device) {
        console.log(JSON.stringify(device));
        if (device.status === 'scanStarted') {
          return;
        }
        deviceCallback(device);
      }, function (err) {
        $ionicPopup.alert({title: 'Err', template: JSON.stringify(err)});
        bluer.stopScans();
        finalCallback();
      }, {
        services: []
      });
    };

    bluer.leAdInfo = function (device) {
      if (device.advertisement) {
        var i,
          allAdInfo = '';
        if (device.advertisement.serviceData) {
          for (i in device.advertisement) {
            if (device.advertisement.hasOwnProperty(i) && i !== "manufacturerData") {
              allAdInfo += i + ": " + JSON.stringify(device.advertisement[i]) + "<br>";
            }
          }
          if (device.advertisement.manufacturerData) {
            allAdInfo += "<br>" + parseAd(window.atob(device.advertisement.manufacturerData));
          }
        } else {
          allAdInfo += parseAd(window.atob(device.advertisement));
        }
        $ionicPopup.alert({title: 'Info for ' + device.name + ' ' + device.address,
          template: '<b>rssi:</b> ' + device.rssi + '<br>' + '<b>advertisement:</b> <br>' + allAdInfo});
      } else {
        $ionicPopup.alert({title: 'Info for ' + device.name + ' ' + device.address,
          template: '<b>rssi:</b> ' + device.rssi + '<br>' + '<b>no advertisement data available</b>'});
      }
    };

    bluer.classicDiscover = function (deviceCallback, finalCallback) {
      window.bluetooth.startDiscovery(function (device) {
        console.log(JSON.stringify(device));
        deviceCallback(device);
      }, function () {
        finalCallback();
      }, function (err) {
        if (err.message === 'Discovery was cancelled.') {
          return;
        }
        $ionicPopup.alert({title: 'Err', template: JSON.stringify(err)});
        bluer.stopScans();
        finalCallback();
      });
    };

    bluer.stopScans = function () {
      if (leAvailable) {
        window.bluetoothle.stopScan(function () {
          console.log("stopped LE scan");
        });
      }
      if (classicAvailable) {
        window.bluetooth.stopDiscovery(function () {
          console.log("stopped classic scan");
        });
      }
    };

    bluer.leInit = function () {
      var promise = $q.defer();
      leAvailable = false;

      if (window.bluetoothle) {
        window.bluetoothle.isEnabled(function (enabled) {
          leAvailable = true;
          if (!enabled || !enabled.isEnabled) {
            if (ionic.Platform.isIOS()) {
              if (iosFailed) {
                $ionicPopup.alert({title: 'Error', template: "You must turn on Bluetooth in your phone's settings."});
              } else {
                iosFailed = true;
              }
            } else {
              window.bluetoothle.enable();
            }
          }
        });

        window.bluetoothle.isInitialized(function (initialized) {
          if (!initialized || !initialized.isInitialized) {
            window.bluetoothle.initialize(promise.resolve, function (err) {
              if (err.error === 'enable') {
                return promise.resolve();
              }
              promise.reject(err);
            });
          } else {
            promise.resolve();
          }
        });

        setTimeout(function () {
          if (!leAvailable) {
            promise.reject("OS too old.");
          }
        }, 1000);
      } else {
        promise.reject("Bluetooth LE plugin not available");
      }

      return promise.promise;
    };

    bluer.classicInit = function () {
      var promise = $q.defer();
      classicAvailable = false;

      if (window.bluetooth) {
        window.bluetooth.isEnabled(function (enabled) {
          classicAvailable = true;
          if (!enabled) {
            window.bluetooth.enable(promise.resolve, promise.reject);
          } else {
            promise.resolve();
          }
        }, promise.reject);
      } else {
        promise.reject("Bluetooth classic plugin not available");
      }

      return promise.promise;
    };

    return bluer;
  }];
  angular.module('mi.app')
    .service('bluething', bluething);
}());
