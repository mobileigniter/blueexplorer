(function () {
  'use strict';
  var ionicPushDec = ['$ionicPush', '$ionicUser', '$q',
    function ($ionicPush, $ionicUser, $q) {
      var ionicPush = {};

      ionicPush.register = function (user, pushFunction) {
        var newUser = $ionicUser.get(),
          d = $q.defer();
        if (user.id) {
          newUser.user_id = user.id;
        }
        // Add some metadata to your user object.
        angular.extend(newUser, user);
        // Identify your user with the Ionic User Service
        $ionicUser.identify(newUser).then(function () {
          $ionicPush.register({
            canShowAlert: true, //Can pushes show an alert on your screen?
            canSetBadge: true, //Can pushes update app icon badges?
            canPlaySound: true, //Can notifications play a sound?
            canRunActionsOnWake: true, //Can run actions outside the app,
            onNotification: pushFunction
          }).then(function (token) {
            d.resolve(token);
          }, function (err) {
            d.reject(err);
          });
        }, function (err) {
          d.reject(err);
        });
        return d.promise;
      };

      return ionicPush;
    }];
  angular.module('ionicPush', ['ionic', 'ionic-service-core', 'ionic-service-push', 'ngCordova'])
    .service('ionicPush', ionicPushDec);
}());
