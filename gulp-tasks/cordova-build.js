var gulp = require('gulp'),
  path = require('path'),
  tap = require('gulp-tap'),
  replace = require('gulp-replace'),
  gulpif = require('gulp-if'),
  useref = require('gulp-useref'),
  uglify = require('gulp-uglify'),
  runSequence = require('run-sequence'),
  fs = require('fs'),
  rename = require('gulp-rename'),
  del = require('del'),
  vinylPaths = require('vinyl-paths'),

  configName = 'config.xml',
  tmpPrefix = 'template-',
  filesToVersion = [
    // maybe if we want to change css, or app.js to edit/restrict states or something
  ],
  version =  {
    iteration: process.env.CORDOVA_CMDLINE.toLowerCase().indexOf('-i') > -1 ? process.env.CORDOVA_CMDLINE.substring(process.env.CORDOVA_CMDLINE.toLowerCase().indexOf('-i') + 3) : 'alpha',
    build: process.env.CORDOVA_CMDLINE.toLowerCase().indexOf('prod') > -1 ? "prod" : 'dev',
    isIOS: process.env.CORDOVA_CMDLINE.toLowerCase().indexOf('ios') > -1
  },
  assetPaths = {
    'ios': {
      'src': [
        path.resolve(__dirname, '../platforms/ios/www/js'),
        path.resolve(__dirname, '../platforms/ios/www/views')
      ],
      'www' : 'platforms/ios/www',
      'compiled' : 'platforms/ios/www/js/-compiled',
      'index' : 'platforms/ios/www/index.html'
    },
    'android': {
      'src': [
        path.resolve(__dirname, '../platforms/android/assets/www/js'),
        path.resolve(__dirname, '../platforms/android/assets/www/views')
      ],
      'www' : 'platforms/android/assets/www',
      'compiled' : 'platforms/android/assets/www/js/-compiled',
      'index' : 'platforms/android/assets/www/index.html'
    }
  },
  deleteFolderRecursive = function (removePath, clear) {
    if (fs.existsSync(removePath)) {
      // don't touch compiled
      if (removePath.indexOf('-compiled') === -1) {
        if (fs.readdirSync(removePath)) {
          fs.readdirSync(removePath).forEach(function (file) {
            // don't delete our compiled js
            var curPath = path.join(removePath, file);
            if (fs.lstatSync(curPath).isDirectory()) { // recurse
              deleteFolderRecursive(curPath, true);
            } else { // delete js files
              // if build is crashing/failing here, it's likely because you need to update node
              // http://davidwalsh.name/upgrade-nodejs
              if (path.parse(curPath).ext === '.js') {
                fs.unlinkSync(curPath);
              }
            }
          });
          // don't delete empty folders && don't remove root folders
          if (fs.readdirSync(removePath).length === 0 && clear) {
            fs.rmdirSync(removePath);
          }
        }
      }
    }
  },
  toggleTempConfig = function (deleteTmp) {
    if (deleteTmp) {
      gulp.src(tmpPrefix + configName)
        .pipe(rename({basename: 'config'}))
        .pipe(gulp.dest('./'));
      return gulp.src(tmpPrefix + configName)
        .pipe(vinylPaths(del));
    }
    if (fs.existsSync(configName)) {
      return gulp.src(configName)
        .pipe(rename({prefix: tmpPrefix}))
        .pipe(gulp.dest('./'));
    }
  };

// make a backup of our config file so we can edit config.xml with version parameters & etc, then after build we'll restore our 'template' config.xml
gulp.task('make-temp-config', function () {
  return toggleTempConfig();
});

// restore our config file
gulp.task('restore-config', function () {
  return toggleTempConfig(true);
});
gulp.task('get-version', function (done) {
  var projectConfig = fs.readFileSync('ionic.project', {encoding: 'utf8'}),
    idRegExp;
  if (version.build === 'prod') {
    idRegExp = /["|']app\_id["|'][^"|']*["|']([^"|']*)["|']/gi;
  } else {
    idRegExp = /["|']dev_app\_id["|'][^"|']*["|']([^"|']*)["|']/gi;
  }
  version.app_id = idRegExp.exec(projectConfig)[1];
  if (!version.app_id) {
    version.app_id = 'com.mobileigniter.test-app';
  }
  console.log('building with app id: ' + version.app_id);
  done();
});
gulp.task('version-app', function () {
  return filesToVersion.forEach(function (file) {
    console.log('versioning', file.src, version.iteration, assetPaths[version.isIOS ? 'ios' : 'android'].www + file.src.substring(0, file.src.lastIndexOf('/')));
    return gulp.src(file.src)
      .pipe(replace(/VERSIONITERATION/g, version.iteration))
      .pipe(replace(/VERSIONENVIRONMENT/g, version.build))
      // .pipe(replace(/VERSIONBUILD/g, version.build[version.build].buildId))
      .pipe(replace(/VERSIONAPPID/g, version.app_id))
      // put file in appropriate directory in platform's www (remove filename itself otherwise gulp spits out EEXISTS -- tries to make dir?)
      .pipe(gulp.dest(assetPaths[version.isIOS ? 'ios' : 'android'].www.split('www')[0] + file.src.substring(0, file.src.lastIndexOf('/'))));
  });
});
gulp.task('update-config', function () {
  return gulp.src(configName)
    .pipe(replace(/VERSIONITERATION/g, version.iteration))
    .pipe(replace(/VERSIONENVIRONMENT/g, version.build))
    // .pipe(replace(/VERSIONBUILD/g, version.build[version.build].buildId))
    .pipe(replace(/VERSIONAPPID/g, version.app_id))
    .pipe(gulp.dest('./'));
});
gulp.task('versioning', function () {
  runSequence('get-version', 'update-config');
});

gulp.task('remove-js-src', function () {
  if (version.isIOS) {//ios
    assetPaths.ios.src.forEach(deleteFolderRecursive);
  } else {//android, presumably
    assetPaths.android.src.forEach(deleteFolderRecursive);
  }
});
gulp.task('uglify-js', function () {
  var assets = useref.assets();
  return gulp.src(assetPaths[version.isIOS ? 'ios' : 'android'].index)
    .pipe(assets)
    .pipe(gulpif(version.build !== 'dev', uglify()))
    .pipe(assets.restore())
    .pipe(useref())
    .pipe(gulp.dest(assetPaths[version.isIOS ? 'ios' : 'android'].www));
});
gulp.task('version-js-build', function () {
  runSequence('remove-js-src', 'uglify-js');
});
