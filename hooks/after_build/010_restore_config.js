#!/usr/bin/env node

/**
 * create backup of config file so we can set version for just this build
 */

var dir = require('../../gulp-tasks/cordova-build.js'),
  runSequence = require('run-sequence');

runSequence(['restore-config']);
